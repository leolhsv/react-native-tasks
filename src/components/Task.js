import React from 'react'
import { StyleSheet, Text, View, TouchableWithoutFeedback } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import moment from 'moment'
import 'moment/locale/pt-br'
import commonStyles from '../commonStyles'

/*
* doneAt = concluido em
* props. = Significa que este componente espera receber alguns parametros:
* doneAt, desc, estimadoAt
*/

export default props => {
    let check = null
    if (props.doneAt !== null) { //se a data da tarefa está diferente de nulo, essa tarefa precisa ser "chekada"
        check = (
            <View style={styles.done}>
                <Icon name='check' size={20} color={commonStyles.colors.secondary} />
            </View>
        )
    } else {
        check = <View style={styles.pending} />
    }

    const descStyle = props.doneAt !== null ? { textDecorationLine: 'line-through' } : {}

    return (
        <View style={styles.container}>


            {/* A função toggleTask está sendo passada assim, pq não é só o evento que está sendo passado,
            mas também o parâmetro id, caso contrário poderia apenas passar a função no onPress */}
            <TouchableWithoutFeedback onPress={() => props.toggleTask(props.id)}>
                {/* aqui é apenas o icone do check */}
                <View style={styles.checkContainer}>{check}</View>
            </TouchableWithoutFeedback>
            
            <View>
                <Text style={[styles.description, descStyle]}>
                    {props.desc}
                </Text>
                <Text style={styles.date}>
                    {moment(props.estimateAt).locale('pt-br').format('ddd, D [de] MMMM')}
                </Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: 10, //Espaçaneto interno, dentro da céula
        flexDirection: 'row', 
        borderBottomWidth: 1, 
        borderColor: '#AAA',
    },
    checkContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '20%', //largura 20%, vai ocupar a linha inteira. Desses 20, ele vai centralizar
    },
    pending: {
        borderWidth: 1, 
        height: 25,
        width: 25,
        borderRadius: 15,
        borderColor: '#555',
    },
    done: {
        height: 25,
        width: 25,
        borderRadius: 15,
        backgroundColor: '#4D7031',
        alignItems: 'center',
        justifyContent: 'center',
    },
    description: {
        fontFamily: commonStyles.fontFamily,
        color: commonStyles.colors.matinText,
        fontSize: 15,
    },
    date: {
        fontFamily: commonStyles.fontFamily,
        color: commonStyles.colors.subText,
        fontSize: 12,
    }
})